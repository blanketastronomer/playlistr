from setuptools import setup, find_packages

try:
    from pypandoc import convert
    read_md = lambda f: convert(f, 'rst')
except ImportError:
    print("WARNING: pypandoc module not found, could not convert Markdown to RST")
    read_md = lambda f: open(f, 'r').read()

setup(
    name='playlistr',
    version='0.1',
    description='A playlist player for OSX and Linux.',
    long_description=read_md('README.md'),
    url='https://gitlab.com/blanketastronomer/playlistr',
    author='Nicholas S.',
    author_email='nicksmrekar@gmail.com',
    license='MIT',
    packages=find_packages(),
    python_requires=">=3.6.5",
    # Packages to be bundled with this package.
    install_requires=[
        'click'
    ],
    # Entry points (commandline executables)
    entry_points={
        'console_scripts': [
            'playlistr = playlistr.cli:cli'
        ]
    }
)
