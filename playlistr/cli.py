import click
import os

@click.group()
@click.version_option(version='0.1')
def cli():
    pass

@cli.command(help-'HELP MESSAGE')
@click.argument('name')
@click.option('--activate-something', is_flag=True, default=True, help='ACTIVATE SOMETHING')
def example_command(name, **kwargs):
    pass
